require("dotenv").config();
const Bungie = require("./Bungie");

/**
 * Due to some issues I am having properly downloading the compressed Manifest,
 * I am currently using the following to download, store and decompress said file.
 *
 * I'm such a newbie!
 */

Bungie.manifestName();

const Traveler = require("the-traveler").default;
const Manifest = require("the-traveler/build/Manifest");
const traveler = new Traveler({
  apikey: process.env.X_API_KEY
});

traveler.destiny2.getDestinyManifest().then(r => {
  traveler.destiny2
    .downloadManifest(
      r.Response.mobileWorldContentPaths.en,
      `./tmp/storage/manifest.content`
    )
    .catch(e => console.log(`Error fetching manifest -> ${e}`));
});

console.log("Moving on to processing SQLite file...");

Bungie.processDB("./tmp/storage/manifest.content");

const axios = require("axios");
const fs = require("fs");

module.exports = {
  plumbing() {
    return axios
      .get("https://destiny.plumbing/")
      .then(r => r.data)
      .catch(e => console.log(`Error fetching from DestinyPlumbing - ${e}`));
  },

  version() {
    return this.plumbing()
      .then(r => r.bungieManifestVersion)
      .catch(e => console.log(`Error fetching from DestinyPlumbing - ${e}`));
  },

  lastUpdated() {
    return this.plumbing()
      .then(r => r.lastUpdated)
      .catch(e => console.log(`Error fetching from DestinyPlumbing - ${e}`));
  },

  en() {
    return this.plumbing()
      .then(r => r.en.raw)
      .catch(e => console.log(`Error fetching from DestinyPlumbing - ${e}`));
  },

  DestinyClassDefinition() {
    return this.en()
      .then(r => axios.get(r.DestinyClassDefinition).then(r => r.data))
      .catch(e => console.log(`Error fetching DestinyClassDefinition - ${e}`));
  }
};

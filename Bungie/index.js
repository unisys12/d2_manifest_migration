const axios = require("axios");
const fs = require("fs");
const sqlite3 = require("sqlite3").verbose();
const mongo = require("../DB");

const db = new sqlite3.Database("./tmp/storage/manifest.content");

const instance = axios.create({
  baseURL: "https://www.bungie.net/",
  timeout: 1000,
  headers: { "X-API-KEY": process.env.X_API_KEY }
});

module.exports = {
  manifestVersion() {
    return instance
      .get("Platform/Destiny2/Manifest/")
      .then(r => r.data.Response.version)
      .catch(e => console.log(`Error fetching Manifest path - ${e}`));
  },

  manifestPath() {
    return instance
      .get("Platform/Destiny2/Manifest/")
      .then(r => r.data.Response.mobileWorldContentPaths.en)
      .catch(e => console.log(`Error fetching Manifest path - ${e}`));
  },

  manifestName() {
    return instance
      .get("Platform/Destiny2/Manifest/")
      .then(r => {
        const path = r.data.Response.mobileWorldContentPaths.en;
        const nameIndex = path.lastIndexOf("/");
        return path.slice(nameIndex).replace("/", "");
      })
      .catch(e => console.log(`Error fetching Manifest path - ${e}`));
  },

  downloadManifest() {
    console.log(`Downloading Manifest from ${this.manifestPath()}`);
    return this.manifestPath().then(x =>
      instance
        .get(`${x}`)
        .then(x => x.data)
        .catch(e => console.log(`Error downloading the Manifest file - ${e}`))
    );
  },

  saveManifest() {
    return this.downloadManifest()
      .then(x => {
        console.log("saving manifest...");
        fs.createWriteStream("./tmp/storage/manifest.content", x);
      })
      .catch(e => console.log(`Error saving Manifest file - ${e}`));
  },

  unzipManifest() {
    if (fs.existsSync("./tmp/storage/manifest.content")) {
      console.log("Unzipping Manifest...");
      fs.createReadStream("./tmp/storage/manifest.content").pipe(
        unzip.Extract({ path: "./tmp/storage/manifest.db" })
      );
    } else {
      console.log(
        "Manifest file to be unzipped. Downloading. Run this method again... for now..."
      );
      this.saveManifest();
    }
  },

  processDB(file) {
    const db = new sqlite3.Database(file);

    db.all(
      "select name from sqlite_master where type='table'",
      (err, tables) => {
        if (err) console.log(`Error fetching table names - ${err}`);
        tables.forEach(t => {
          this.processTables(t);
        });
      }
    );
  },

  processTables(table) {
    console.log(`Processing ${table.name}`);
    let rows = new Promise((resolve, reject) => {
      db.all(`select * from ${table.name}`, (err, rows) => {
        if (err) return reject(err);
        resolve(rows);
      });
    });

    let data;
    rows
      .then(async x => {
        let batch = [];
        console.log(`Read ${x.length} rows from ${table.name}`);
        for (let r = 0; r < x.length; r++) {
          data = JSON.parse(x[r].json);
          data.id = data.hash || data.statId || rows[r].id;
          // Check for redacted/blank rows
          if (!data.id && x.id !== 0) {
            console.log(`No ID found in ${table.name}`);
          }
          if (data.Bungie) {
            return;
          }
          batch.push(data);
        }
        await mongo.insert(table.name, batch);
        console.log(`Processed ${batch.length} rows for ${table.name}`);
        console.log(" ");
      })
      .catch(e => console.log(`Error processing row -> ${e}`));
  }
};

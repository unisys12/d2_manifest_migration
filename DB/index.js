const mongo = require("mongodb");

module.exports = {
  async insert(collectionName, batch) {
    try {
      let collection = await loadCollection(collectionName);
      collection.insertMany(batch);
    } catch (e) {
      console.log(`Error performing replaceOne -> ${e}`);
    }
  }
};

async function loadCollection(collectionName) {
  const client = await mongo.MongoClient.connect(process.env.DB_URL, {
    useNewUrlParser: true
  });
  return client.db(process.env.DB_NAME).collection(collectionName);
}
